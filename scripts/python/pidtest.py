#!/usr/bin/env python3

import recordreplay

import os
import re
import sys
import time


if __name__ == '__main__':
    
    assert len(sys.argv) == 2
    startup_zeros = int(sys.argv[1])
    startup_zeros = 'true' if startup_zeros else 'false'
    recordreplay.log(f'startup_zeros={startup_zeros}')
    
    path = 'fgaddon-git/Harrier-GR3/Systems/Autopilot.xml'
    with open(path) as f:
        text = f.read()
    text2 = re.sub(
            '[<]startup-zeros[>][^<]+[<]/startup-zeros[>]',
            f'<startup-zeros>{startup_zeros}</startup-zeros>',
            text,
            )
    if text2 != text:
        with open(path, 'w') as f:
            f.write(text2)
        recordreplay.log(f'changed path={path}')
    
    command = (''
            './build-walk/fgfs.exe-run.sh'
            ' --airport=lowi'
            ' --state=approach'
            ' --prop:/autopilot/settings/vertical-speed-fpm=500'
            ' --prop:bool:/sim/replay/record-continuous=true'
            )
    
    t = time.time()
    fg = recordreplay.Fg('harrier-gr3', f'{command}')
    fg.waitfor('/sim/fdm-initialized', 1, timeout=45)
    fg.fg['/controls/flight/elevator'] = 0
    time.sleep(5)
    fg.fg['/autopilot/locks/altitude'] = 'vertical-speed-hold'
    time.sleep(20)
    fg.fg['/sim/replay/record-continuous'] = 0
    path = f'{recordreplay.g_tapedir}/{fg.aircraft}-continuous.fgtape'
    
    os.system(f'ls -lL {path}')
    s = os.stat(path, follow_symlinks=True)
    assert s.st_mtime > t
    path2 = os.readlink(path)
    recordreplay.log(f'path={path} path2={path2}')
    
    fg.close()

